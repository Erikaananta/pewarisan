/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kesiswaan;

import javax.swing.JOptionPane;

/**
 *
 * @author ERIKA
 */
class Kesiswaan {

    private String ketua, wakil, sekretaris, bendahara, anggota;

    public void setKetua(String ketua) {
        this.ketua = ketua;
    }

    public void setWakil(String wakil) {
        this.wakil = wakil;
    }

    public void setSekretaris(String sekretaris) {
        this.sekretaris = sekretaris;
    }

    public void setBendahara(String bendahara) {
        this.bendahara = bendahara;
    }

    public void setAnggota(String anggota) {
        this.anggota = anggota;
    }

    public String getKetua() {
        return ketua;
    }

    public String getWakil() {
        return wakil;
    }

    public String getSekretaris() {
        return sekretaris;
    }

    public String getBendahara() {
        return bendahara;
    }

    public String getAnggota() {
        return anggota;
    }

    public void tampilkanNama() {
        System.out.println("Tampilkan Nama " + getAnggota());
    }

    public static void main(String[] args) {
        System.out.println("========= Nama Nama Struktur Kurikulum =========");
        String Ketua = JOptionPane.showInputDialog("Input nama : ");
        System.out.println("Nama Ketua : " + Ketua);
        String wakil = JOptionPane.showInputDialog("Input nama : ");
        System.out.println("Nama wakil : " + wakil);
        String sekretaris = JOptionPane.showInputDialog("Input nama : ");
        System.out.println("Nama wakil : " + sekretaris);
        String bendahara = JOptionPane.showInputDialog("Input nama : ");
        System.out.println("Nama wakil : " + bendahara);
    }

}

class Guru extends Kesiswaan {

    private String Guru;

    public void setGuru(String Guru) {
        this.Guru = Guru;
    }

    public String getGuru() {
        return Guru;
    }

    @Override
    public void tampilkanNama() {
        super.tampilkanNama(); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Nama Guru " + getGuru());

    }
}

class tampilkanNama {

    public static void main(String[] args) {
        Guru b = new Guru();
        b.tampilkanNama();

    }
}
